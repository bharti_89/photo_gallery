from django.shortcuts import render_to_response, render, HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponseRedirect, HttpResponse
from login.models import User, Album, Photo
from login.forms import UploadForm, AlbumForm


@csrf_exempt
def signup(request):
    state = ''
    if request.POST:
        email = request.POST.get('email')
        username = request.POST.get('username')
        password = request.POST.get('password')
        u = User.objects.filter(username__iexact=username)
        if u:
            state = "Username already exists"
            return render_to_response('login/signup.html', {'state': state})
        else:
            User.objects.create(username=username, email=email, password=password)
        return HttpResponseRedirect(reverse('login'))
    else:
        return render_to_response('login/signup.html')

# import pdb; pdb.set_trace()

@csrf_exempt
def match(request):
    state = ''
    username = password = ''
    if request.method == 'POST':
        if request.POST.get('username'):
            username = request.POST.get('username')
        if request.POST.get('password'):
            password = request.POST.get('password')
            x=User.objects.get(username__iexact=username)
            print x.user_id
            # User.objects.get(user_id=3).delete()
            # User.objects.get(user_id=4).delete()
            # Album.objects.get(album_id=16).delete()
            # Album.objects.get(album_id=18).delete()

    # Retrieve records from table
        if username:
            u = User.objects.filter(username__iexact=username)
            print u
            # import pdb;pdb.set_trace()
            # Matching user_id and password
            if u:
                if u[0].username.lower() == username.lower() and u[0].password == password:
                    state = "Login successful"
                    url = reverse('create_album', args=(u[0].user_id,))
                    return HttpResponseRedirect(url)
                else:
                    state = "Login failed"
            else:
                state = "Login failed"
        return render_to_response('login/login1.html', {'state':state} )
    else:
        return render_to_response('login/login1.html', {'state': state})


@csrf_exempt
def create_album(request, user_id):
    if request.method == 'POST':
        return create_album_post(request, user_id)
    else:
        form = AlbumForm()
        u = User.objects.filter(user_id=user_id)
        album_list = Album.objects.filter(user=u[0])
        # import pdb;pdb.set_trace()
        print (album_list)
        return render(request, 'login/album.html',{'album_list': album_list, 'form': form, 'user_id': user_id})


def create_album_post(request, user_id):
    form = AlbumForm()
    name = request.POST.get('name')
    u = User.objects.get(user_id=user_id)
    Album.objects.create(user=u, album_name=name)
    album_list = Album.objects.filter(user=u)
    return render(request, 'login/album.html', {'album_list': album_list, 'form': form, 'user_id': user_id})


@csrf_exempt
def upload_image(request, user_id, album_name):
    if request.method == 'POST':
        return upload_image_post(request, user_id, album_name)
    else:
        form = UploadForm() # An empty, unbound form
        a = Album.objects.filter(album_name=album_name, user=user_id)
        # Retrieving image list
        # import pdb;pdb.set_trace()
        image_list = Photo.objects.filter(album=a[0].album_id)
        return render(request, 'login/list.html',{'image_list': image_list, 'form': form, 'album_name': album_name, 'user_id': user_id})


def upload_image_post(request, user_id, album_name):
    form = UploadForm(request.POST, request.FILES)
    if form.is_valid():
        a = Album.objects.filter(album_name=album_name, user=user_id)
        newdoc = Photo(album=a[0], image=request.FILES['docfile'])
        newdoc.save()
        image_list = Photo.objects.filter(album=a[0].album_id)
        return render(request, 'login/list.html', {'image_list': image_list, 'form': form, 'album_name': album_name, 'user_id': user_id})


@csrf_exempt
def delete_image(request):
    id=''
    if request.method == 'POST':
        form = UploadForm(request.POST)
        id = request.POST.get('id')
        album_name = request.POST.get('album_name')
        user_id = request.POST.get('user_id')
        # album_name = request.POST.get('album_name')
        Photo.objects.get(image_id=id).delete()
        a = Album.objects.filter(album_name=album_name, user_id=user_id)
        # Load documents for the list page
        image_list = Photo.objects.filter(album=a[0])
        return HttpResponseRedirect(reverse('upload', args=(user_id, album_name,)))


@csrf_exempt
def delete_album(request):
    id = ''
    if request.method == 'POST':
        form = AlbumForm(request.POST)
        id = request.POST.get('id')
        user_id = request.POST.get('user_id')
        Album.objects.get(album_id=id).delete()
    # import pdb;pdb.set_trace()
    u = User.objects.filter(user_id=user_id)
    album_list = Album.objects.filter(user=u[0])
    return HttpResponseRedirect(reverse('create_album', args=(user_id,)))


